/*
 *  Video Lecture: 22
 *  Programmer: Arif Butt
 *  Course: System Programming with Linux
 *  myshellv1.c: 
 *  main() displays a prompt, receives a string from keyboard, pass it to tokenize()
 *  tokenize() allocates dynamic memory and tokenize the string and return a char**
 *  main() then pass the tokenized string to execute() which calls fork and exec
 *  finally main() again displays the prompt and waits for next command string
 *   Limitations:
 *   if user press enter without any input the program gives sigsegv 
 *   if user give only spaces and press enter it gives sigsegv
 *   if user press ctrl+D it give sigsegv
 *   however if you give spaces and give a cmd and press enter it works
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <pwd.h>
#include <glob.h>
#include <signal.h>
#include <readline/readline.h>
#include "pipes.h"
#include "internal.h"
#include "history.h"
#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "\nWaqarShell:- "
#define NR_JOBS 20

int execute(char * arglist[]);
char ** tokenize(char * cmdline);
char ** tokenizePipe(char * cmdline);
int execpipe (char ** argv1, char ** argv2);
char * read_cmd(char * , FILE * );
char inputFileName[100], outputFileName[100];
int backupInput, backupOutput;
void decodeHistory(char * cmdline);
void sigCHLDFunc(int signal);
void updateHistory(char* arglist);
int isInternalCommand(char * command);
int countLines(char * name);
void executeInternalCommand(char** command);
int get_next_job_id() ;

int background=0;
int currectProcess=0;



struct job_temp{
  int pid;
  int job_id;
  char command[100];
};

struct job_temp jobsarray[NR_JOBS];
int maxJobs=0;

char * cmdline;
  
int main(int argc,char ** argv) {

  if(argc==3&&strcmp(argv[1],"-f")==0){
    int file=open(argv[2],O_RDONLY);
    if(file<0){
      printf("file not found.Exiting....\n");
      return 0;
    }
    dup2(file,0);
    close(file);

  }else if(argc!=1){
      printf("Invalid arguments. Exiting....\n");
      return 0;
    
  }
  rl_bind_key('\t', rl_complete);


  int i;
  for (i = 0; i < NR_JOBS; i++) {
      jobsarray[i].pid = -1;
  }

  backupInput = dup(0);
  backupOutput = dup(1);

  inputFileName[0] = '\0';
  outputFileName[0] = '\0';

  char ** arglist;
  char * prompt = PROMPT;
  while ((cmdline = read_cmd(prompt, stdin)) != NULL) {
    int semicolon=0;
      for(int i=0;cmdline[i]!='\0';i++){
          if(cmdline[i]==';'){
            semicolon++;
          }
      }
    
    char * cmdLineSplit=strtok(cmdline,";");
    //char * asd=strtok(NULL,";");
    //if(asd==NULL)
 //   if(semicolon==0)  
  //  updateHistory(cmdline);
  
    while(cmdLineSplit!=NULL){
      // if(cmdLineSplit[0]=='#' ||cmdLineSplit[0]=='\0' ){
      //      cmdLineSplit=strtok(NULL,";");
      //   continue;
      // }
      background=0;
      currectProcess=0;
      int pipeCount=0;
      for(int i=0;cmdLineSplit[i]!='\0';i++){
          if(cmdLineSplit[i]=='|'){
            pipeCount++;
          }
      }
      if(pipeCount>0){
        int pid=fork();
        if(pid==0){
          runPipedCommands(cmdLineSplit,pipeCount,inputFileName,outputFileName);
          }else{
           wait(NULL);      
         } 

        
      }else{
      //  if(semicolon>0)
  
        decodeHistory(cmdLineSplit);
        updateHistory(cmdLineSplit);
        
        if ((arglist = tokenize(cmdLineSplit)) != NULL) {
          execute(arglist);
          //  need to free arglist
          for (int j = 0; j < MAXARGS + 1; j++)
            free(arglist[j]);
          free(arglist);
        }

      }
    cmdLineSplit=strtok(NULL,";");
 
    } //end of while loop
    free(cmdline);
       
  }
  printf("\n");
  close(backupInput);
  close(backupOutput);
  return 0;
}





void sigCHLDFunc(int signal){
  currectProcess--;
  int  pid = wait(NULL);
  int jobid=-1,i;
  for(i=0;i<NR_JOBS;i++){
   // printf("pid:%d--%d\n",jobsarray[i].pid,jobsarray[i].jobid);
    if(pid==jobsarray[i].pid){
        jobsarray[i].pid=-1;
        jobid=jobsarray[i].job_id;
        break;
      }
  }
  if(jobid==-1)
    return;
  printf("[%d]  %d\n",jobid ,pid);
    

}
int execute(char * arglist[]) {
  if(isInternalCommand(arglist[0])){
    executeInternalCommand(arglist);
    return 0;
  }
  int fd1 = -1, fd2 = -1;
  if (inputFileName[0] != '\0') {
    fd1 = open(inputFileName, O_RDONLY);
    dup2(fd1, 0);
    inputFileName[0] = '\0';
  }
  if (outputFileName[0] != '\0') {

    fd2 = open(outputFileName, O_WRONLY | O_CREAT, 0755);
    dup2(fd2, 1);
    outputFileName[0] = '\0';
  }

  int status;
  int cpid = fork();
  switch (cpid) {
  case -1:
    perror("fork failed");
    exit(1);
  case 0:
    execvp(arglist[0], arglist);
    perror("Command not found...");
    exit(1);
  default:
    dup2(backupInput, 0);
    dup2(backupOutput, 1);
    if(background==0){
        //signal(SIGCHLD,SIG_IGN); 
        signal(SIGCHLD,SIG_DFL);
        waitpid(cpid, & status, 0);
       // printf("child exited with status %d \n", status >> 8);
    }else{
        struct job_temp tmp;
        tmp.pid=cpid;
        tmp.job_id=get_next_job_id();
        strcpy(tmp.command,cmdline);
        //tmp.command=arglist;
        jobsarray[tmp.job_id-1]=tmp;

        printf("[%d] %d\n",tmp.job_id,cpid);
        signal(SIGCHLD, sigCHLDFunc); 
     //   signal(SIGCHLD,SIG_IGN); 
    }
    if (fd1 != -1)
      close(fd1);
    if (fd2 != -1)
      close(fd2);
    return 0;
  }
}



char ** tokenizePipe(char * cmdline) {
  //allocate memory
  int iRedirect = 0, oRedirect = 0;
  char ** arglist = (char ** ) malloc(sizeof(char * ) * (MAXARGS + 1));
  for (int j = 0; j < MAXARGS + 1; j++) {
    arglist[j] = (char * ) malloc(sizeof(char) * ARGLEN);
    bzero(arglist[j], ARGLEN);
  }
  if (cmdline[0] == '\0') //if user has entered nothing and pressed enter key
    return NULL;
  int argnum = 0; //slots used
  char * cp = cmdline; // pos in string
  char * start;
  int len;
  while ( * cp != '\0') {
    while ( * cp == ' ' || * cp == '\t'||* cp == '|') //skip leading spaces
      cp++;
    start = cp; //start of the word
    len = 1;
    //find the end of the word
    while ( * ++cp != '\0' && !( * cp == '|'))
      len++;
    if(*cp=='|')
     len--;

    strncpy(arglist[argnum], start, len);
    arglist[argnum][len] = '\0';
    if (iRedirect == 1) {
      strncpy(inputFileName, arglist[argnum], len + 1);
      iRedirect = -1;
      argnum -= 2;
    }
    if (oRedirect == 1) {
      strncpy(outputFileName, arglist[argnum], len + 1);
      oRedirect = -1;
      argnum -= 2;
    }

    if (strcmp(arglist[argnum], "<") == 0 && iRedirect != -1) {
      iRedirect = 1;
    }
    if (strcmp(arglist[argnum], ">") == 0 && oRedirect != -1) {
      oRedirect = 1;
    }

    argnum++;
  }
  if(arglist[argnum-1][0]=='&'){
    background=1;
    arglist--;
  }
  arglist[argnum] = "\0";
  return arglist;
}


char ** tokenize(char * cmdline) {
  //allocate memory
  int iRedirect = 0, oRedirect = 0;
  char ** arglist = (char ** ) malloc(sizeof(char * ) * (MAXARGS + 1));
  for (int j = 0; j < MAXARGS + 1; j++) {
    arglist[j] = (char * ) malloc(sizeof(char) * ARGLEN);
    bzero(arglist[j], ARGLEN);
  }
  if (cmdline[0] == '\0') //if user has entered nothing and pressed enter key
    return NULL;
  int argnum = 0; //slots used
  char * cp = cmdline; // pos in string
  char * start;
  int len;
  while ( * cp != '\0') {
    while ( * cp == ' ' || * cp == '\t') //skip leading spaces
      cp++;
    start = cp; //start of the word
    len = 1;
    //find the end of the word
    while ( * ++cp != '\0' && !( * cp == ' ' || * cp == '\t'))
      len++;
    if(*cp==' ' && *(cp+1)=='\0')
     *cp='\0';

    strncpy(arglist[argnum], start, len);
    arglist[argnum][len] = '\0';
    if (iRedirect == 1) {
      strncpy(inputFileName, arglist[argnum], len + 1);
      iRedirect = -1;
      argnum -= 2;
    }
    if (oRedirect == 1) {
      strncpy(outputFileName, arglist[argnum], len + 1);
      oRedirect = -1;
      argnum -= 2;
    }

    if (strcmp(arglist[argnum], "<") == 0 && iRedirect != -1) {
      iRedirect = 1;
    }
    if (strcmp(arglist[argnum], ">") == 0 && oRedirect != -1) {
      oRedirect = 1;
    }
    if(arglist[argnum][0]=='&'){
      background=1;
      argnum--;
    }

    argnum++;
  }
  
  arglist[argnum] = NULL;
  return arglist;
}

char * read_cmd(char * prompt, FILE * fp) {
  //printf("%s", prompt);
  // int c; //input character
  // int pos = 0; //position of character in cmdline
   char * cmdline = (char * ) malloc(sizeof(char) * MAX_LEN);
  // while ((c = getc(fp)) != EOF) {
  //   if (c == '\n')
  //     break;
  //   cmdline[pos++] = c;
  // }
  //these two lines are added, in case user press ctrl+d to exit the shell
//  if (c == EOF && pos == 0)
 //   return NULL;
  //cmdline[pos] = '\0';
    cmdline = readline(prompt);
     if (!cmdline)
            return NULL;

  return cmdline;
}