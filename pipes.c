#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <pwd.h>
#include <glob.h>
#include <signal.h>
#include <readline/readline.h>
#include "pipes.h"
char ** tokenize(char * cmdline);
char ** tokenizePipe(char * cmdline);
void decodeHistory(char * cmdline);

void runPipedCommands(char * input, int numPipes,char * inputFileName, char * outputFileName) {

    int status;
    int i = 0;
   // pid_t pid;
    char ** arglist;

    int pipefds[2*numPipes];
    for(i = 0; i < (numPipes); i++){
        if(pipe(pipefds + i*2) < 0) {
            perror("couldn't pipe");
            exit(EXIT_FAILURE);
        }
    }
    int j = 0;
    char **command=tokenizePipe(input);
    int z=0;
    FILE* file=fopen("temp.txt","w");
    fclose(file);
   // fputs("4", file);
    int pid[10];        
    while(j<=2*numPipes) {
                  inputFileName[0]='\0';
                  outputFileName[0]='\0';
                  decodeHistory(command[z]);
                  arglist = tokenize(command[z]);   
                  pid[j] = fork();
                  

                  if(pid[j] == 0) {
                      //input redirection if applicable

                        int fd1 = -1, fd2 = -1;
                        if (inputFileName[0] != '\0') {
                          fd1 = open(inputFileName, O_RDONLY);
                          dup2(fd1, 0);
                          inputFileName[0] = '\0';
                        }
                        if (outputFileName[0] != '\0') {

                          fd2 = open(outputFileName, O_WRONLY | O_CREAT, 0755);
                          dup2(fd2, 1);
                          outputFileName[0] = '\0';
                        }
                      
                      //if not last command
                      if(j<2*numPipes){
                          if(dup2(pipefds[j + 1], 1) < 0){
                              perror("dup2");
                              exit(EXIT_FAILURE);
                          }
                      }

                      //if not first command&& j!= 2*numPipes
                      if(j != 0 ){
                          if(dup2(pipefds[j-2], 0) < 0){
                              perror("dup2");
                              exit(EXIT_FAILURE);

                          }
                      }

                      for(i = 0; i < 2*numPipes; i++){
                          close(pipefds[i]);
                      }

                      if(execvp(arglist[0],arglist) < 0 ){
                              perror("error");
                              _exit(EXIT_FAILURE);
                      }
                  } else if(pid < 0){
                          perror("error");
                          _exit(EXIT_FAILURE);
                  } else{
                    FILE* file=fopen("temp.txt","a");
                      fprintf(file, "J2=%d\n",j);
                      //fputs("5", file);
                      fclose(file);
                     // waitpid(pid[j],NULL,NULL);
                     
                  }
                  
        z++;
        j+=2;
    
    }
    //printf("asd=%d\n",numPipes);
    for(i = 0; i < 2 * numPipes; i++){
        close(pipefds[i]);
    }
    for(i = 0; i < numPipes + 1; i++)
       wait(&status);

}

