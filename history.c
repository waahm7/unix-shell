#include "history.h"
#define MAX_LEN 512

int countLines(char * name){
  int linesCount=0;
  FILE *file = fopen(name, "r");
    char chr = getc(file);
    while (chr != EOF){
        if (chr == '\n')
            linesCount = linesCount + 1;
        chr = getc(file);
    }
    fclose(file); 
  return linesCount;
}

void decodeHistory(char * cmdline){
  if(cmdline[0]!='!')
    return ;
        int linesCount=countLines("/Users/waqar/work/asgn_2_bcsf16m525/history.txt");
        int lineNo;
        if(cmdline[1]=='-'){
          if(cmdline[2]=='1' && cmdline[3]=='0'){
            lineNo=10;
          }else{
            lineNo=cmdline[2]-'0';
          }
        } else if (cmdline[1]=='1' && cmdline[2]=='0') {
          lineNo=1;
        } else {
          int tmp=cmdline[1]-'0';
          lineNo=linesCount-tmp+1;
          
        }

        FILE* file = fopen("/Users/waqar/work/asgn_2_bcsf16m525/history.txt", "r");
        int i = 0, check = 0;
        while (fgets(cmdline, sizeof(char)*MAX_LEN, file)) {
            i++;
            if(i == lineNo ){
              check = 1;
                break;
            }
        }
     
        fclose(file);

        if(check==0){
         // return ("echo \"no command ")
          //printf("No Command Found..\n");
          return ;
          //continue;
        }
        char *pos;
        if ((pos=strchr(cmdline, '\n')) != NULL)
          *pos = '\0';
        
}

void updateHistory(char* arglist){
  
    FILE *f = fopen("/Users/waqar/work/asgn_2_bcsf16m525/history.txt","a");
    fclose(f);
    
    int linesCount=countLines("/Users/waqar/work/asgn_2_bcsf16m525/history.txt");
    
    rename("/Users/waqar/work/asgn_2_bcsf16m525/history.txt","/Users/waqar/work/asgn_2_bcsf16m525/.historyTemp");
      
    FILE *outputfp = fopen("/Users/waqar/work/asgn_2_bcsf16m525/history.txt", "a");
    fprintf(outputfp, "%s\n", &arglist[0]);

    FILE* file = fopen("/Users/waqar/work/asgn_2_bcsf16m525/.historyTemp", "r");
    char line[256];

    if(linesCount>=10){ //limit is met     
      
      int i = 0;
      int check=0;
      while (fgets(line, sizeof(line), file)) {
          i++;
          if(i == 10 )
              break;
          char *pos;
      if ((pos=strchr(line, '\n')) != NULL)
          *pos = '\0';
          if(check==0 && strcmp(line,arglist)==0){
            continue;
          }
          fprintf(outputfp, "%s\n", &line[0]);
          check=1;
      }

    } else {
      int check=0;
      while (fgets(line, sizeof(line), file)!=NULL) {
        char *pos;
      if ((pos=strchr(line, '\n')) != NULL)
          *pos = '\0';
        if(check==0 && strcmp(line,arglist)==0){
          continue;
        }
          fprintf(outputfp, "%s\n", &line[0]);
          check=1;
      }
    }

    remove("/Users/waqar/work/asgn_2_bcsf16m525/.historyTemp");

    fclose(outputfp);
    fclose(file);

}
