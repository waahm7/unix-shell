all: myexe
CC=gcc
INCLUDES=.
LIBS = -lreadline
OBJS= history.o internal.o pipes.o myshell.o

myexe: $(OBJS)
	$(CC) -o myexe $(OBJS)  $(LIBS)
myshell.o: myshell.c 
	$(CC) -c myshell.c  

history.o: history.c 
	$(CC) -c history.c  
pipes.o: pipes.c
	$(CC) -c pipes.c  
internal.o: internal.c
	$(CC) -c internal.c

clean:
	-@rm -f $(OBJS)

