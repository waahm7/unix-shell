#include "internal.h"
#define NR_JOBS 20

int isInternalCommand(char * command){
  if(strcmp(command,"help")==0)
    return 1;
  if(strcmp(command,"cd")==0)
    return 1;
  if(strcmp(command,"kill")==0)
    return 1;
  if(strcmp(command,"jobs")==0)
    return 1;
  if(strcmp(command,"exit")==0)
    return 1;  
  return 0;
}

struct job_temp{
  int pid;
  int job_id;
  char command[100];
};


extern struct job_temp jobsarray[NR_JOBS];
int get_next_job_id() {
    int i;

    for (i = 0; i < NR_JOBS; i++) {
        if (jobsarray[i].pid == -1) {
            return i+1;
        }
    }

    return -1;
}

void executeInternalCommand(char** command){
    if(strcmp(command[0],"help")==0){
      printf("%s\n","Available built in commands");
      printf("%s\n","1-cd path (change directory)");
      printf("%s\n","2-kill jobnumber (terminate the process running in background)");
      printf("%s\n","3-jobs (returns a list of commands running in backgroun)");
      printf("%s\n","4-exit (exit the shell)");
      
      return ;
    }
  if(strcmp(command[0],"cd")==0){
    chdir(command[1]);
    return ;
  }
  if(strcmp(command[0],"kill")==0){
    int num=-1;
    if(command[1]==NULL){
     printf("please enter job id\n");
      return; 
      
    }
    if(command[1][0]>='1'&&command[1][0]<='9'){
      num=command[1][0]-'0';
      if(command[1][1]>='1'&&command[1][1]<='9'){
        num=num*10;
        num+=command[1][1]-'0';
      }
    }

    if(num<1||num>NR_JOBS || jobsarray[num-1].pid==-1){
      printf("invalid job id\n");
      return; 
    }
    kill(jobsarray[num-1].pid,SIGKILL);
    printf("[%d] terminated %d %s\n",num,jobsarray[num-1].pid,jobsarray[num-1].command);
    
    jobsarray[num-1].pid=-1;
    return ;
  }
  if(strcmp(command[0],"jobs")==0){
    int i=0;
    for(;i<NR_JOBS;i++){
      if(jobsarray[i].pid!=-1){
        printf("[%d] running %s\n",jobsarray[i].job_id,jobsarray[i].command);
      }
    }

    return ;
  }
  if(strcmp(command[0],"exit")==0){
    exit(0); 
  }
}
